package socaldesignautomation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class SocalLinksNavigation {
	WebDriver driver;
	JSONParser parser = new JSONParser();
	
	String firefox_font_family_1 = ("Arial, Helvetica, \"sans-serif\"").replaceAll("\"", "");
	String firefox_font_family_2 = ("Interstate, Helvetica, \"sans-serif\"").replaceAll("\"", "");
	@BeforeTest(alwaysRun = true)
	@Parameters({"browser","environment"})
	public void setUpTest(String browser,String environment) throws FileNotFoundException, IOException, ParseException {
		switch (browser) {
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			break;
		case "Edge":
			System.setProperty("webdriver.edge.driver", "C:\\Drivers\\msedgedriver.exe");
			driver = new EdgeDriver();
			break;
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
			break;
		}
			switch(environment) {
			case "production":
				 //Object obj1 = parser.parse(new FileReader("C:/Users/user/git/socal_repository/Socal Automation/prodUrl.json"));
				// JSONObject jsonObject1 = (JSONObject)obj1;
				 JSONObject jsonObject1 = (JSONObject)parser.parse(new FileReader("D:/Eclipse-Workspace/Socal-Automation/prodUrl.json"));
				 driver.get((String)jsonObject1.get("SocalLinksNavigation")); 
				 break;
			case "staging":
				 //Object obj2 = parser.parse(new FileReader("C:/Users/user/git/socal_repository/Socal Automation/stagingUrl.json"));
				 //JSONObject jsonObject2 = (JSONObject)obj2;
				 JSONObject jsonObject2 = (JSONObject)parser.parse(new FileReader("D:/Eclipse-Workspace/Socal-Automation/stagingUrl.json"));
				 driver.get((String)jsonObject2.get("SocalLinksNavigation"));
				 break;
			}
		//driver.get("https://www.socalgas.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void UtilityNavLinksNavigation() {
		// Get all the links on the page
		List<WebElement> UtilityNavAllLinks = driver.findElements(By.xpath("//ul[@class='utility-nav__list']/li/a"));
		Reporter.log("Total Size of Utility Nav Links:" + UtilityNavAllLinks.size());
		for (int i = 0; i < UtilityNavAllLinks.size(); i++) {
			WebElement element = UtilityNavAllLinks.get(i);
			Reporter.log(element.getText());
			String utilityUrl = element.getAttribute("href");
			verifyLink(utilityUrl);
		}
	}

	@Test
	public void GlobalNavigation() {
		List<WebElement> globalLink = driver.findElements(By.xpath("//a[@class='main-nav__link']"));
		Reporter.log("Total no of links Available: " + globalLink.size());
		for (WebElement link : globalLink) {
			Reporter.log(link.getText());
			String globalUrl = link.getAttribute("href");
			verifyLink(globalUrl);
		}
	}

	@Test(enabled=false)//for next sprint
	public void PayBillsNavigation() {
		Actions action = new Actions(driver);
		WebElement payBill = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[1]"));
		action.moveToElement(payBill);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[2]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void ScheduleServiceNavigation() {
		Actions action = new Actions(driver);
		WebElement ScheduleServiceNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[2]"));
		action.moveToElement(ScheduleServiceNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[3]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void StaySafeNavigation() {
		Actions action = new Actions(driver);
		WebElement StaySafeNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[3]"));
		action.moveToElement(StaySafeNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[4]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void SaveMoneyAndEnergyNavigation() {
		Actions action = new Actions(driver);
		WebElement SaveMoneyAndEnergyNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[4]"));
		action.moveToElement(SaveMoneyAndEnergyNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[5]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void ForYourBusinessNavigation() {
		Actions action = new Actions(driver);
		WebElement ForYourBusinessNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[5]"));
		action.moveToElement(ForYourBusinessNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[6]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void SmartEnergyNavigation() {
		Actions action = new Actions(driver);
		WebElement SmartEnergyNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[6]"));
		action.moveToElement(SmartEnergyNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[7]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test(enabled=false)//for next sprint
	public void OurCommunityNavigation() {
		Actions action = new Actions(driver);
		WebElement OurCommunityNavi = driver.findElement(By.xpath("(//a[@class='main-nav__link'])[7]"));
		action.moveToElement(OurCommunityNavi);
		action.build().perform();
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
		List<WebElement> subMenu = driver
				.findElements(By.xpath("//div[@class=\"main-nav__wrapper\"]/ul/li[8]/ul/ul/li/a"));
		Reporter.log("Total no of links Available: " + subMenu.size());
		for (int i = 0; i < subMenu.size(); i++) {
			WebElement element = subMenu.get(i);
			Reporter.log(element.getText());
			String subMenuUrl = element.getAttribute("href");
			verifyLink(subMenuUrl);
		}

	}

	@Test
	public void FooterLinkNavigation() {
		List<WebElement> FooterLinks = driver
				.findElements(By.xpath("//*[@id=\"footer\"]/div/div[1]/div[1]/div[1]/ul/li/a"));
		Reporter.log("Total Size of Footer Links:" + FooterLinks.size());
		for (int i = 0; i < FooterLinks.size(); i++) {
			WebElement element = FooterLinks.get(i);
			Reporter.log(element.getText());
			String footerUrl = element.getAttribute("href");
			verifyLink(footerUrl);
		}
	}

	@Test
	public void ConnectWithUsNavigation() {
		List<WebElement> SocalLinkNavi = driver.findElements(By.xpath("//a[@class='site-footer__social-link']"));
		Reporter.log("Total Size of Social Links:" + SocalLinkNavi.size());
		for (int i = 0; i < SocalLinkNavi.size(); i++) {
			WebElement element = SocalLinkNavi.get(i);
			Reporter.log(element.getText());
			String SocalUrl = element.getAttribute("href");
			verifyLink(SocalUrl);
		}
	}

	@Test
	public void FooterLogoNavigation() {
		WebElement footerLogoImg = driver.findElement(By.xpath("//div[@class='site-footer__logo']/a"));
		
		if (footerLogoImg.isDisplayed() && footerLogoImg.isEnabled()) {
			footerLogoImg.click();
			Reporter.log("Footer Logo image is Displayed & Enabled");
		}
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
	}

	public void verifyLink(String urlLink) {
		// Sometimes we may face exception "java.net.MalformedURLException". Keep the
		// code in try catch block to continue the broken link analysis
		try {
			// Use URL Class - Create object of the URL Class and pass the urlLink as
			// parameter
			URL link = new URL(urlLink);
			// Create a connection using URL object (i.e., link)
			HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
			// Set the timeout for 2 seconds
			httpConn.setConnectTimeout(10000);
			// connect using connect method
			httpConn.connect();
			// use getResponseCode() to get the response code.
			if (httpConn.getResponseCode() == 200) {
				Reporter.log(urlLink + " - " + httpConn.getResponseMessage());
			}
			if (httpConn.getResponseCode() == 201) {
				Reporter.log(urlLink + " - " + httpConn.getResponseMessage());
			}
			if (httpConn.getResponseCode() == 404) {
				Reporter.log(urlLink + " - " + httpConn.getResponseMessage());
			}
		} catch (Exception e) {
			e.printStackTrace();

		}
	}

	@AfterTest
	public void terminateBrowser() {
		driver.close();
	}
}
