package socaldesignautomation;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Reporter;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class LinkTestStaleElementSoln {
	WebDriver driver;
	JSONParser parser = new JSONParser();
	
	String firefox_font_family_1 = ("Arial, Helvetica, \"sans-serif\"").replaceAll("\"", "");
	String firefox_font_family_2 = ("Interstate, Helvetica, \"sans-serif\"").replaceAll("\"", "");
	@BeforeTest(alwaysRun = true)
	@Parameters({"browser","environment"})
	public void setUpTest(String browser,String environment) throws FileNotFoundException, IOException, ParseException {
		switch (browser) {
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "C:\\Drivers\\chromedriver.exe");
			driver = new ChromeDriver();
			break;
		case "Edge":
			System.setProperty("webdriver.edge.driver", "C:\\Drivers\\msedgedriver.exe");
			driver = new EdgeDriver();
			break;
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", "C:\\Drivers\\geckodriver.exe");
			driver = new FirefoxDriver();
			break;
		}
			switch(environment) {
			case "production":
				 //Object obj1 = parser.parse(new FileReader("C:/Users/user/git/socal_repository/Socal Automation/prodUrl.json"));
				// JSONObject jsonObject1 = (JSONObject)obj1;
				 JSONObject jsonObject1 = (JSONObject)parser.parse(new FileReader("D:/Eclipse-Workspace/Socal-Automation/prodUrl.json"));
				 driver.get((String)jsonObject1.get("SocalLinksNavigation")); 
				 break;
			case "staging":
				 //Object obj2 = parser.parse(new FileReader("C:/Users/user/git/socal_repository/Socal Automation/stagingUrl.json"));
				 //JSONObject jsonObject2 = (JSONObject)obj2;
				 JSONObject jsonObject2 = (JSONObject)parser.parse(new FileReader("D:/Eclipse-Workspace/Socal-Automation/stagingUrl.json"));
				 driver.get((String)jsonObject2.get("SocalLinksNavigation"));
				 break;
			}
		//driver.get("https://www.socalgas.com/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}


	@Test(enabled = false)
	public void InsidePayBillLinks() throws InterruptedException {

		Actions action = new Actions(driver);
		String PayBillXpath = ".//a[@class='main-nav__link'][text()='Pay Bill']";

		String myAccountXpath = ".//a[@href=\"/pay-bill/my-account\"][text()='My Account']";
		String waysTpPayXpath = ".//a[@href='/pay-bill/ways-to-pay'][text()='Ways to Pay']";
		String paymentLocationXpath = ".//a[@href='/pay-bill/ways-to-pay/payment-locations'][text()='Payment Locations']";
		String timetoPayXpath = ".//a[@href='/pay-bill/need-more-time-to-pay'][text()='Need More Time to Pay?']";
		String understandBillXpath = ".//a[@href='/pay-bill/understanding-your-bill'][text()='Understanding Your Bill']";

		String[] str = { myAccountXpath, waysTpPayXpath, paymentLocationXpath, timetoPayXpath, understandBillXpath };

		for (int i = 0; i < str.length; i++) {
			try {
				action.moveToElement(driver.findElement(By.xpath(PayBillXpath))).build().perform();
				driver.findElement(By.xpath(str[i])).click();
				System.out.println(driver.getTitle());
				driver.navigate().back();
			} catch (Exception e) {
				Thread.sleep(2000);
				action.moveToElement(driver.findElement(By.xpath(PayBillXpath))).build().perform();
				driver.findElement(By.xpath(str[i])).click();
				System.out.println(driver.getTitle());
				driver.navigate().back();
			}
		}

	}

	@Test
	public void UtilityNavLinksNavigation() throws InterruptedException {

		List<WebElement> UtilityNavAllLinks = driver.findElements(By.xpath("//ul[@class='utility-nav__list']/li/a"));
		Reporter.log("Total Size of Utility Nav Links:" + UtilityNavAllLinks.size());

		String hrefHolder[] = new String[UtilityNavAllLinks.size()];

		for (int i = 0; i < UtilityNavAllLinks.size(); i++) {
			WebElement element = UtilityNavAllLinks.get(i);
			Reporter.log(element.getText());
			hrefHolder[i] = element.getAttribute("href");
		}
		verifyLink(hrefHolder);
	}

	@Test
	public void GlobalNavigation() {
		List<WebElement> globalLink = driver.findElements(By.xpath("//a[@class='main-nav__link']"));
		Reporter.log("Total no of links Available: " + globalLink.size());
		String hrefHolder[] = new String[globalLink.size()];
		for (int i = 0; i < globalLink.size(); i++) {
			WebElement element = globalLink.get(i);
			Reporter.log(element.getText());
			hrefHolder[i] = element.getAttribute("href");
		}
		verifyLink(hrefHolder);
	}

	public void verifyLink(String a[]) {

		for (String s : a) {
			try {
				System.out.println(s + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				URL link = new URL(s);
				HttpURLConnection httpConn = (HttpURLConnection) link.openConnection();
				httpConn.setConnectTimeout(10000);
				httpConn.connect();
				if (httpConn.getResponseCode() == 200) {
					Reporter.log(s + " - " + httpConn.getResponseMessage());
				}
				if (httpConn.getResponseCode() == 201) {
					Reporter.log(s + " - " + httpConn.getResponseMessage());
				}
				if (httpConn.getResponseCode() == 404) {
					Reporter.log(s + " - " + httpConn.getResponseMessage());
				}
			} catch (Exception e) {
				e.printStackTrace();

			}
		}
	}
	@AfterTest
	public void terminateBrowser() {
		driver.close();
	}
	
}
